from django.test import TestCase
from django.db import models


class StateModelTests(TestCase):
    def test_state_model_has_name_field(self):
        try:
            from saunter_rest.models import State

            name = State.name
            self.assertIsInstance(
                name.field,
                models.CharField,
                msg="State.name should be a character field",
            )
        except ModuleNotFoundError:
            self.fail("Could not find 'saunter_rest.models'")
        except ImportError:
            self.fail("Could not find 'saunter_rest.models.State'")
        except AttributeError:
            self.fail("Could not find 'State.name'")

    def test_state_model_has_name_with_max_length_20_characters(self):
        try:
            from saunter_rest.models import State

            name = State.name
            self.assertEqual(
                name.field.max_length,
                20,
                msg="The max length of State.name should be 20",
            )
        except ModuleNotFoundError:
            self.fail("Could not find 'saunter_rest.models'")
        except ImportError:
            self.fail("Could not find 'saunter_rest.models.State'")
        except AttributeError:
            self.fail("Could not find 'State.name'")

    def test_state_model_has_name_that_is_not_nullable(self):
        try:
            from saunter_rest.models import State

            name = State.name
            self.assertFalse(
                name.field.null,
                msg="State.name should not be nullable",
            )
        except ModuleNotFoundError:
            self.fail("Could not find 'saunter_rest.models'")
        except ImportError:
            self.fail("Could not find 'saunter_rest.models.State'")
        except AttributeError:
            self.fail("Could not find 'State.name'")

    def test_state_model_has_name_that_is_not_blank(self):
        try:
            from saunter_rest.models import State

            name = State.name
            self.assertFalse(
                name.field.blank,
                msg="State.name should not be allowed a blank value",
            )
        except ModuleNotFoundError:
            self.fail("Could not find 'saunter_rest.models.State'")
        except AttributeError:
            self.fail("Could not find 'State.name'")

    def test_state_model_has_abbreviation_field(self):
        try:
            from saunter_rest.models import State

            abbreviation = State.abbreviation
            self.assertIsInstance(
                abbreviation.field,
                models.CharField,
                msg="State.abbreviation should be a character field",
            )
        except ModuleNotFoundError:
            self.fail("Could not find 'saunter_rest.models'")
        except ImportError:
            self.fail("Could not find 'saunter_rest.models.State'")
        except AttributeError:
            self.fail("Could not find 'State.abbreviation'")

    def test_state_model_has_abbreviation_that_is_not_nullable(self):
        try:
            from saunter_rest.models import State

            abbreviation = State.abbreviation
            self.assertFalse(
                abbreviation.field.null,
                msg="State.abbreviation should not be nullable",
            )
        except ModuleNotFoundError:
            self.fail("Could not find 'saunter_rest.models'")
        except ImportError:
            self.fail("Could not find 'saunter_rest.models.State'")
        except AttributeError:
            self.fail("Could not find 'State.abbreviation'")

    def test_state_model_has_abbreviation_that_is_not_blank(self):
        try:
            from saunter_rest.models import State

            abbreviation = State.abbreviation
            self.assertFalse(
                abbreviation.field.blank,
                msg="State.abbreviation should not be allowed a blank value",
            )
        except ModuleNotFoundError:
            self.fail("Could not find 'saunter_rest.models.State'")
        except AttributeError:
            self.fail("Could not find 'State.abbreviation'")

    def test_state_model_has_abbreviation_with_max_length_2_characters(self):
        try:
            from saunter_rest.models import State

            abbreviation = State.abbreviation
            self.assertEqual(
                abbreviation.field.max_length,
                2,
                msg="The max length of State.abbreviation should be 20",
            )
        except ModuleNotFoundError:
            self.fail("Could not find 'saunter_rest.models'")
        except ImportError:
            self.fail("Could not find 'saunter_rest.models.State'")
        except AttributeError:
            self.fail("Could not find 'State.abbreviation'")


class LocationModelTests(TestCase):
    def test_location_model_has_name_field(self):
        try:
            from saunter_rest.models import Location

            name = Location.name
            self.assertIsInstance(
                name.field,
                models.CharField,
                msg="Location.name should be a character field",
            )
        except ModuleNotFoundError:
            self.fail("Could not find 'saunter_rest.models'")
        except ImportError:
            self.fail("Could not find 'saunter_rest.models.Location'")
        except AttributeError:
            self.fail("Could not find 'Location.name'")

    def test_location_model_has_name_with_max_length_200_characters(self):
        try:
            from saunter_rest.models import Location

            name = Location.name
            self.assertEqual(
                name.field.max_length,
                200,
                msg="The max length of Location.name should be 200",
            )
        except ModuleNotFoundError:
            self.fail("Could not find 'saunter_rest.models'")
        except ImportError:
            self.fail("Could not find 'saunter_rest.models.Location'")
        except AttributeError:
            self.fail("Could not find 'Location.name'")

    def test_location_model_has_name_that_is_not_nullable(self):
        try:
            from saunter_rest.models import Location

            name = Location.name
            self.assertFalse(
                name.field.null,
                msg="Location.name should not be nullable",
            )
        except ModuleNotFoundError:
            self.fail("Could not find 'saunter_rest.models'")
        except ImportError:
            self.fail("Could not find 'saunter_rest.models.Location'")
        except AttributeError:
            self.fail("Could not find 'Location.name'")

    def test_location_model_has_name_that_is_not_blank(self):
        try:
            from saunter_rest.models import Location

            name = Location.name
            self.assertFalse(
                name.field.blank,
                msg="Location.name should not be allowed a blank value",
            )
        except ModuleNotFoundError:
            self.fail("Could not find 'saunter_rest.models.Location'")
        except AttributeError:
            self.fail("Could not find 'Location.name'")

    def test_location_model_has_city_field(self):
        try:
            from saunter_rest.models import Location

            city = Location.city
            self.assertIsInstance(
                city.field,
                models.CharField,
                msg="Location.city should be a character field",
            )
        except ModuleNotFoundError:
            self.fail("Could not find 'saunter_rest.models'")
        except ImportError:
            self.fail("Could not find 'saunter_rest.models.Location'")
        except AttributeError:
            self.fail("Could not find 'Location.city'")

    def test_location_model_has_city_with_max_length_200_characters(self):
        try:
            from saunter_rest.models import Location

            city = Location.city
            self.assertEqual(
                city.field.max_length,
                200,
                msg="The max length of Location.city should be 200",
            )
        except ModuleNotFoundError:
            self.fail("Could not find 'saunter_rest.models'")
        except ImportError:
            self.fail("Could not find 'saunter_rest.models.Location'")
        except AttributeError:
            self.fail("Could not find 'Location.city'")

    def test_location_model_has_city_that_is_not_nullable(self):
        try:
            from saunter_rest.models import Location

            city = Location.city
            self.assertFalse(
                city.field.null,
                msg="Location.city should not be nullable",
            )
        except ModuleNotFoundError:
            self.fail("Could not find 'saunter_rest.models'")
        except ImportError:
            self.fail("Could not find 'saunter_rest.models.Location'")
        except AttributeError:
            self.fail("Could not find 'Location.city'")

    def test_location_model_has_city_that_is_not_blank(self):
        try:
            from saunter_rest.models import Location

            city = Location.city
            self.assertFalse(
                city.field.blank,
                msg="Location.city should not be allowed a blank value",
            )
        except ModuleNotFoundError:
            self.fail("Could not find 'saunter_rest.models.Location'")
        except AttributeError:
            self.fail("Could not find 'Location.city'")

    def test_location_model_has_state_related_name_of_location(self):
        try:
            from saunter_rest.models import Location

            state = Location.state
            self.assertEqual(
                state.field.related_query_name(),
                "location",
                msg="Location.state should have a related name of 'location'",
            )
        except ModuleNotFoundError:
            self.fail("Could not find 'saunter_rest.models'")
        except ImportError:
            self.fail("Could not find 'saunter_rest.models.Location'")
        except AttributeError:
            self.fail("Could not find 'Location.state'")


class TripModelTests(TestCase):
    def test_trip_model_has_title_field(self):
        try:
            from saunter_rest.models import Trip

            title = Trip.title
            self.assertIsInstance(
                title.field,
                models.CharField,
                msg="Trip.title should be a character field",
            )
        except ModuleNotFoundError:
            self.fail("Could not find 'saunter_rest.models'")
        except ImportError:
            self.fail("Could not find 'saunter_rest.models.Trip'")
        except AttributeError:
            self.fail("Could not find 'Location.title'")

    def test_trip_model_has_title_with_max_length_200_characters(self):
        try:
            from saunter_rest.models import Trip

            title = Trip.title
            self.assertEqual(
                title.field.max_length,
                200,
                msg="The max length of Trip.title should be 200",
            )
        except ModuleNotFoundError:
            self.fail("Could not find 'saunter_rest.models'")
        except ImportError:
            self.fail("Could not find 'saunter_rest.models.Trip'")
        except AttributeError:
            self.fail("Could not find 'Trip.title'")

    def test_trip_model_has_title_that_is_not_nullable(self):
        try:
            from saunter_rest.models import Trip

            title = Trip.title
            self.assertFalse(
                title.field.null,
                msg="Trip.title should not be nullable",
            )
        except ModuleNotFoundError:
            self.fail("Could not find 'saunter_rest.models'")
        except ImportError:
            self.fail("Could not find 'saunter_rest.models.Trip'")
        except AttributeError:
            self.fail("Could not find 'Trip.title'")

    def test_trip_model_has_title_that_is_not_blank(self):
        try:
            from saunter_rest.models import Trip

            title = Trip.title
            self.assertFalse(
                title.field.blank,
                msg="Trip.title should not be allowed a blank value",
            )
        except ModuleNotFoundError:
            self.fail("Could not find 'saunter_rest.models.Trip'")
        except AttributeError:
            self.fail("Could not find 'Trip.title'")

    def test_trip_model_has_date_field(self):
        try:
            from saunter_rest.models import Trip

            date = Trip.date
            self.assertIsInstance(
                date.field,
                models.DateField,
                msg="Trip.date should be a date field",
            )
        except ModuleNotFoundError:
            self.fail("Could not find 'saunter_rest.models'")
        except ImportError:
            self.fail("Could not find 'saunter_rest.models.Trip'")
        except AttributeError:
            self.fail("Could not find 'Trip.date'")

    def test_trip_model_has_total_cost_field(self):
        try:
            from saunter_rest.models import Trip

            total_cost = Trip.total_cost
            self.assertIsInstance(
                total_cost.field,
                models.PositiveBigIntegerField,
                msg="Trip.total_cost should be an integer field",
            )
        except ModuleNotFoundError:
            self.fail("Could not find 'saunter_rest.models'")
        except ImportError:
            self.fail("Could not find 'saunter_rest.models.Trip'")
        except AttributeError:
            self.fail("Could not find 'Trip.total_cost'")

    def test_trip_model_has_location_related_name_of_trip(self):
        try:
            from saunter_rest.models import Trip

            location = Trip.location
            self.assertEqual(
                location.field.related_query_name(),
                "trip",
                msg="Trip.location should have a related name of 'trip'",
            )
        except ModuleNotFoundError:
            self.fail("Could not find 'saunter_rest.models'")
        except ImportError:
            self.fail("Could not find 'saunter_rest.models.Trip'")
        except AttributeError:
            self.fail("Could not find 'Trip.location'")


class ReviewModelTest(TestCase):
    def test_review_model_has_trip_related_name_of_review(self):
        try:
            from saunter_rest.models import Review

            trip = Review.trip
            self.assertEqual(
                trip.field.related_query_name(),
                "review",
                msg="Review.trip should have a related name of 'review'",
            )
        except ModuleNotFoundError:
            self.fail("Could not find 'saunter_rest.models'")
        except ImportError:
            self.fail("Could not find 'saunter_rest.models.Review'")
        except AttributeError:
            self.fail("Could not find 'Review.trip'")

    def test_review_model_has_lodging_field(self):
        try:
            from saunter_rest.models import Review

            lodging = Review.lodging
            self.assertIsInstance(
                lodging.field,
                models.CharField,
                msg="Review.lodging should be a character field",
            )
        except ModuleNotFoundError:
            self.fail("Could not find 'saunter_rest.models'")
        except ImportError:
            self.fail("Could not find 'saunter_rest.models.Review'")
        except AttributeError:
            self.fail("Could not find 'Review.lodging'")

    def test_review_model_has_lodging_with_max_length_200_characters(self):
        try:
            from saunter_rest.models import Review

            lodging = Review.lodging
            self.assertEqual(
                lodging.field.max_length,
                200,
                msg="The max length of Review.lodging should be 200",
            )
        except ModuleNotFoundError:
            self.fail("Could not find 'saunter_rest.models'")
        except ImportError:
            self.fail("Could not find 'saunter_rest.models.Review'")
        except AttributeError:
            self.fail("Could not find 'Review.lodging'")

    def test_review_model_has_lodging_that_is_not_nullable(self):
        try:
            from saunter_rest.models import Review

            lodging = Review.lodging
            self.assertFalse(
                lodging.field.null,
                msg="Review.name should not be nullable",
            )
        except ModuleNotFoundError:
            self.fail("Could not find 'saunter_rest.models'")
        except ImportError:
            self.fail("Could not find 'saunter_rest.models.Review'")
        except AttributeError:
            self.fail("Could not find 'Review.lodging'")

    def test_review_model_has_lodging_that_is_not_blank(self):
        try:
            from saunter_rest.models import Review

            lodging = Review.lodging
            self.assertFalse(
                lodging.field.blank,
                msg="State.lodging should not be allowed a blank value",
            )
        except ModuleNotFoundError:
            self.fail("Could not find 'saunter_rest.models.Review'")
        except AttributeError:
            self.fail("Could not find 'Location.lodging'")
