from django.apps import AppConfig


class SaunterRestConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "saunter_rest"
