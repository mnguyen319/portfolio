# May 31st, 2022
 Successfully set up the Docker file and run the server. Update the name of the profile.

# June 1st, 2022
Successfully set up the React container in the Dockerfile.

# June 2nd, 2022
Created Django Project and Django App in the saunter-api container. Set up the models.

# June 3rd, 2022
Met some issues setting up the api database. Found there's something wrong with the Dockerfile. Tried to solve that.

# June 7th, 2022
The Dockerfile issues are solved. Updated the user model and built the apis for locations, trips, reviews, and ratings. Also built the url file.

# June 8th, 2022
Working on the front-end part. Design on the main page and nav bar.
Has some questions about token.

# June 9th, 2022 - June 10th, 2022
Worked on the login and sign up page. Got some help from Curtis and finally resolved the issue. Working on the Form Page.

# June 13th, 2022 - June 16th, 2022
Talked with teammates, and everyone needs to take some time working on the algorithm. Thus, no really working on the project for the first 4 days. Picked up some stuffs on the last day. Continue working on the front-end forms page.

# June 21st, 2022 - June 22nd, 2022
Finished the forms page. Focused on the CSS part to make the page looks better.

# June 23rd, 2022
Write unit test on the backend side. 

# June 24th, 2022
Figured out some issues on the database, asked for help on storing external data to the database. Doing some debugging stuff and merge everyone's work together.