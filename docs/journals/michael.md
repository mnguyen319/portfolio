# 5/31/2022
Entire team worked together to get initial project running on Richard's computer. Docker is now working properly.
# 6/01/2022
Two team members missing. Spent the time working on getting the react set up. Got it working on macs but it no longer works on PC. We will be unable to work on the project properly until it gets resolved.
# no idea
divvied up work, redid the same page and app 3 times. All is pain. I feel so useless.
# 6/07/2022
Got search working, beat all the bugs out of docker.
# 6/08/2022
Redid search entirely to a much less awful version and went slowly mad because lowercase and lowerCase are not the same thing.
# 6/09/2022
Reduced load upon user with larger data sets. Commented code to make it modular.
# 6/10/2022-6/20/2022
Did redundant work on blog view and create/update form, made trip forms, studied for practice exam. Debugged so many 500 errors I want to scream. Redid index.js
Added missing view properties. Debugged Type errors until I was blue in the face from things being handed objects.
Learned about not giving usernames to front end and how models can contain sub entries that aren't defined in the model.
# 6/21/2022
Redid links, added and removed placeholders
# 6/22/2022
added gunicorn and made citestbranch, read heroku, ci, and cd documentation. Wrote and rewrote tests. Settled on placeholder test. Docker Broke
# 6/23/2022
Learned I was not supposed to be using heroku's documentation. Learned about and configured black. Changed settings files and focused on flake. Changed enviroment variables. Made search work with blogs and apis, removed all placeholder values. Redid models to handle bugs and type errors. Added and changed views properties. Merged in the ci testing branch. Fixed several 405 and similar errors. Docker updated and broke
# 6/24/2022
It's all a blur. Fixed docker. Integrated and merged everyone's seperate branches. Recruited curtis to beat 500 errors into submission. Redid all the links into not localhost. Reverted into older commits. Created new merge branches for everyone and made multiple functions play nice. made delete functions. Integratted state API and solved keys.py error. Wrote all my blogs based on my commits since I forgot about them until now.