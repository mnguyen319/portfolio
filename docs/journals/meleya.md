## 31-MAY-2022
Worked on setting up the docker compose up and running as a group. 

## 02-JUNE-2022
Worked on the writing out models as a group. Then attempted to render the location model with states and abbreviations. 

## 06/07-JUNE-2022
Worked on sign on page with Aden. The goal is to create a sign on page and after a user creates an account, redirect the page to the main page. The url would be something like /main/username instead of a random number. Attempted to install bootstrap package on new branch. Ended up installing it on the wrong package.json(there are two of them). Then run into "6 high severity vulnerabilities" error. Nothing was pushed so I undid the changes I have made thus far. Still have the same error. 

## 08-JUNE-2022
Error persisting, "6 high severity vulnerabilities" caused the docker container to exit. With the help from Curtis, we deleted the node_modules and rebuilt the docker. But the docker was not working properly and taking a long time to give any kind of response. Curtis gave me instructions to follow after updating docker. Docker has trouble updating or uninstalling.
 
I teamed up with Aden to work off of her computer so there wouldn't be too much time wasted debugging docker. We worked on the sign up page to allow new users to sign up and redirect the user to the home page. Then worked on the BlogForm to render a simple form. Implemented a bootstrap but needs to be edited.

## 20-JUNE-2022
Got on zoom with Aden, worked on css and rendering the form for the blog. 

## 21-JUNE-2022
Created views for Location and State. Attempted to create a table inside the terminal.

## 22-JUNE-2022
The goal for today was to customize the create blog where when a user fills out the form for the “Author” it will have a drop-down option to pick their username as author or display_name as an author. Same for the “Trip” field to have a drop-down option where the user picks which trip they want their blog to be about. Aden and I successfully were to pass the trip data to the create blog page. We tested it by creating a trip on the admin side because we didn't have the create trip code on our branch. 

But we were unsuccessful with the “Author” field to pass the username or display_name. By informing the team we decided to change the model and make migrations. On the review model, we changed the author property value to charField as it makes more sense for the user to choose how they want to be represented. While we are at it we figured to add a description field that will render on the main page inside the cards instead of the whole blog. We also removed the display_name from the user model as it serves no purpose. 

Once we got the create blog to work properly, we moved to authentication. The create blog form will only be available to users who are logged in. 

LEARN: today I have a better understanding of the if/else using question mark and colon Curtis used on the login/logout authentication. We used it again for creating a blog with the help of Jeff. 

## 23-JUNE-2022
Started off by pair programming with Aden as the navigator to redirect the user to logIn page instead of “NOOO” when attempting to access a restricted page. Then we switched roles and worked on the login and sign-up page to look the same. Then created a link for the user to create a blog form once signed up. Also added a logout button once signed in. The logout button will redirect to the home page.

Was having difficulties with showing the title and description from the created blog on the card found on the main page. We were able to debug this issue but will still need to link the title to the detail page where that actual page renders. 

With Jeff, we created a working branch from aden-branch and pulled from the main branch. We decided to accept all changes and commented out the code that belong to Michael until we communicate and have another understanding. 

Aden, Mya, and I work on the landing page once logged in. On that page will be buttons that will redirect the user to see, lists of trips, lists of blogs, create a trip, and create a blog. 

Communicated with Richard and where he wants his weather API to render. Decided on the detail page of the blog. It's supposed to tell the reader what the weather is based on the location of the blog they are reading. 
While working on the detail page with Aden, we couldn’t figure out how to fetch data from the review data by its id. The goal is when selecting a blog it would actually open that specific page. We spent the rest of the night searching and trying different forms of syntax to the data by the id. 

## 24-JUNE-2022
LEARN: today I learned about prop drilling while working on the detail page. And model view control in react which is sort of similar to a template, model, and view in Django. 

Got the detail page working to allow users to click on the read me button from the main page and redirect them to that specific page to read the entire blog. Today was filled with fixing bugs caused by merging branches or pulling new data. 
Added a link on the create trip that links to the location form. Implemented the submit button the create a new location to redirect to the create trip form. Fixed logged-in page to redirect to the homepage instead of “you are logged in the page”. Filled out my journal to the best of my ability from what I remember. 
