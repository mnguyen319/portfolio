## Create New User

* **Method**: `post`
* **Path**: /api/users

Input:

```json
{
    'username': str,
    'email': email,
    'password': str,
    'avatar': img,
}
```

Output:
```json
{
    'user id':int,
    'username': str,
    'email': email,
    'password': str,
    'avatar': img,
}
```
Creating a new user account uses user input data to create an account.
All this data is saved to the database which then returns all of the data with the new ID.
## Delete User Account

* **Method**: `delete`
* **Path**: /api/users

Input:

```json
{
    'username': str,
    'email': email,
    'password': str,
}
```

Output:
```json
{
    href: link
}
```
Takes current and authorized user, and deletes data and all the blogs associated with it.

**BLOGS**

## Create a New blog

* **Method**: `post`
* **Path**: /api/blogs/new
Input:
```json
{
    'location tag':str:fk,
    'pictures': img,
    'content': str,
    'title':str,
    'rating': int,
    'user id' : int:pk

}
```
Output:
```json
{'location tag':str(fk),
    'pictures': img,
    'content': str,
    'title':str,
    'rating': int,
    'blog id':int
    }
```
Creating a new blog uses currently logged in user id and user input to create a blog. All this data is saved to the database which then returns all of the data with a blog ID except user id.

## View a Blog
* **Method** `get`
* **Path**: /api/blogs/{int:pk}
Input:
```json
{
    'blog id':int
}
```
Output:
```json
{
    'location tag':str(fk),
    'pictures': img,
    'content': str,
    'title':str,
    'rating': int,
    'user id': int:fk
}
```
Takes a targeted blog ID and returns all available data including a forigne key to the user id of the user who created the blog.

## List Blogs by User
* **Method** `get`
* **Path**: /api/blogs/users/{int:fk}/
Input:
```json
{
    'user id':int
}
```
Output:
```json
{
    'blog list':user id{list[blog id{title:str}]}
}
```
Takes a targeted user id and lists all associated blogs displaying the blog titles. 

## Update a Blog
* **Method** `put`
* **Path**: /api/blogs/{int:pk}/
Input:
```json
{
    'blog id': int{pk},
    'location tag':str:fk,
    'pictures': img,
    'content': str,
    'title':str,
    'rating': int,
    'user id':int{pk}

}
```
Output:
```json
{
    'location tag':str:fk,
    'pictures': img,
    'content': str,
    'title':str,
    'rating': int,
}
```
Takes current blog page and sends in user input to overwrite databse entires and returns the new values.
## Delete a Blog
* **Method** `delete`
* **Path**: /api/blogs/{int:pk}/
Input:
```json
{
    'user id':int{pk}
    'blog id': int
}
```
Output:
```json
{
    href: link
}
```
Takes current blog id if the user id is authorized and deletes data associated with it.

<!-- **TRIP PLAN**

## Create A Trip Plan

* **Method**: `post`
* **Path**: /api/plans/new

Input:

```json
{
    'dates':date
    'location':href(str),
    'hotels':href(location),
    'restaurants':href(location),
    'transportation':href(location),
    'total budget':int,
    'user id':int{pk},
    'add users':int{fk}
    'title':str
}
```

Output:
```json
{
    'dates':date
    'location':href(str),
    'hotels':href(location),
    'restaurants':href(location),
    'transportation':href(location),
    'total budget':int,
    'add user':int{fk},
    'plan id':int
}
```
Creating a new trip plan uses currently logged in user id, user date input, and selected location. This data is then checked against data sources and collected date is saved to the database which is then sent to the user.

## View A Trip Plan

* **Method**: `get`
* **Path**: /api/plans/{int:pk}

Input:

```json
{
    
    'user id':int{pk},
    'plan id':int{pk}
}
```

Output:
```json
{
    'dates':date
    'location':href(str),
    'hotels':href(location),
    'restaurants':href(location),
    'transportation':href(location),
    'total budget':int,
    'user id':int{fk}   
}
```
Takes in a currently logged in user id and a selected plan returning all stored data as well as the other user id's authorized for plan access.

## Edit A Trip Plan

* **Method**: `put`
* **Path**: /api/plans/{int:pk}

Input:

```json
{
    'dates':date
    'location':href(str),
    'hotels':href(location),
    'restaurants':href(location),
    'transportation':href(location),
    'total budget':int,
    'user id':int{pk}
}
```

Output:
```json
{
    'dates':date
    'location':href(str),
    'hotels':href(location),
    'restaurants':href(location),
    'transportation':href(location),
    'total budget':int,
    'user id':int{pk}   
}
```
Takes current plan page and sends in user input to overwrite databse entires, updates links, and returns the new values.

## Delete A Trip Plan

* **Method**: `delete`
* **Path**: /api/plans/{int:pk}

Input:

```json
{
    
    'user id':int{pk},
    'plan id':int{pk}
}
```

Output:
```json
{
    href: main page
}
```
Takes current plan id if the user id is authorized and deletes data associated with it.


## List Your Trip Plan

* **Method**: `get`
* **Path**: /api/plans/user/{int:pk}

Input:

```json
{
    'user id':int{pk}
}
```

Output:
```json
{
    'plan list':list[plan id{title:str}]  
}
```

Takes in current user id and retrieves all associed plan id's displaying titles. -->
