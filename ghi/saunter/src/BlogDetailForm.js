import { useState, useEffect } from "react";

function BlogDetailForm(props) {
  const [review, setBlogs] = useState([]);
  useEffect(() => {
    const fetchBlog = async () => {
      const blogUrl = `http://localhost:8000/api/reviews/${props.reviewId}`;
      const response = await fetch(blogUrl);
      if (response.ok) {
        const blogData = await response.json();
        setBlogs(blogData.review);
        console.log("***", blogData.review);
      }
    };
    fetchBlog();
  }, [props.reviewId]);

  return (
    <div className="card-group">
      <div className="card">
        <div className="card-body">
          <h5 className="card-title"> Writer: {review.author}</h5>
          <p className="card-text"> {review.content}</p>
          <img
            className="card-img-bottom"
            src={review.pictures}
            alt=""
          />
        </div>
      </div>
    </div>
  );
}

export default BlogDetailForm;
