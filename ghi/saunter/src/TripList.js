import { useState, useEffect } from "react";
import { Link } from "react-router-dom";

function TripListForm(props) {
  const token = props.token;
  const [trips, setTrips] = useState([]);
  useEffect(() => {
    const fetchTrip = async () => {
      const tripUrl = `${process.env.REACT_APP_ACCOUNTS_HOST}/api/trips/`;
      const response = await fetch(tripUrl);
      if (response.ok) {
        const tripData = await response.json();
        setTrips(tripData.trips);
        console.log(tripData.trips);
      }
    };
    fetchTrip();
  }, []);
  if (!token) {
    return (
      <div className="px-4 py-5 my-5 text-center">
        You do not have access to this page, please
        <Link to="/login"> login </Link>
        to view this content.
      </div>
    );
  }
  return (
    <div className="px-4 py-5 my-5 text-center">
      <div className="container">
        <h2>Trips</h2>
        <div className="row">
          {trips.map((trip) => {
            return (
              <div key={trip.id} className="card mb-3 shadow">
                <div className="card-body">
                  <h5 className="card-title">{trip.title}</h5>
                  <h6 className="card-text">{trip.location.name}</h6>
                  <p className="card-subtitle mb-2 text-muted">{trip.date}</p>
                  <p className="card-subtitle mb-2 text-muted">
                    {"$" + trip.total_cost}
                  </p>
                </div>
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
}

export default TripListForm;
