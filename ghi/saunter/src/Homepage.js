import React from "react";
import "./App.css";
import { Link, useNavigate } from "react-router-dom";

function Homepage(props) {
  const token = props.token;
  const navigate = useNavigate();
  const createTrip = () => {
    navigate("/newtrip");
  };
  const createBlog = () => {
    navigate("/createblog");
  };
  const viewTrips = () => {
    navigate("/yourtrips");
  };
  const viewBlogs = () => {
    navigate("/reviews");
  };
  if (!token) {
    return (
      <div className="px-4 py-5 my-5 text-center">
        You do not have access to this page. Please
        <Link to="/login"> login </Link>
        to view this content.
      </div>
    );
  }
  return (
    <div>
      <div className="px-4 py-5 my-5 text-center gap-4">
        <h1 className="display-5 fw-bold">Saunter your way on over!</h1>
        <div className="d-grid gap-5">
          <div>
            <button className="p-2 bg-light border" onClick={createTrip}>
              Create a trip experience
            </button>
          </div>
          <div>
            <button className="p-2 bg-light border" onClick={createBlog}>
              Create a review of your trip
            </button>
          </div>
          <div>
            <button className="p-2 bg-light border" onClick={viewTrips}>
              My trips
            </button>
          </div>
          <div>
            <button className="p-2 bg-light border" onClick={viewBlogs}>
              My your reviews
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Homepage;
