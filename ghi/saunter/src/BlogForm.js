import React from "react";
import { Link, Navigate } from "react-router-dom";

class BlogForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      author: "",
      description: "",
      trip: [],
      content: "",
      lodging: "",
      rec_restaurants: "",
      pictures: "",
      selectedTrip: { title: "" },
      created: false,
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChangeAuthor = this.handleChangeAuthor.bind(this);
    this.handleChangeTrip = this.handleChangeTrip.bind(this);
    this.handleChangeDescription = this.handleChangeDescription.bind(this);
    this.handleChangeContent = this.handleChangeContent.bind(this);
    this.handleChangeLodging = this.handleChangeLodging.bind(this);
    this.handleChangeRecRestaurants =
      this.handleChangeRecRestaurants.bind(this);
    this.handleChangePictures = this.handleChangePictures.bind(this);
  }

  async componentDidMount() {
    const url = `${process.env.REACT_APP_ACCOUNTS_HOST}/api/trips/`;

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      this.setState({ trip: data.trips });
    }
  }

  async handleSubmit(event) {
    event.preventDefault();
    const data = { ...this.state };
    delete data.review;
    data.trip = data.selectedTrip;
    delete data.selectedTrip;
    delete data.created;
    console.log(data);

    const reviewURL = `${process.env.REACT_APP_ACCOUNTS_HOST}/api/reviews/`;
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      credentials: "include",
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(reviewURL, fetchConfig);
    if (response.ok) {
      const newReview = await response.json();
      console.log(newReview);
      this.setState({
        author: "",
        trip: [],
        description: "",
        content: "",
        lodging: "",
        rec_restaurants: "",
        pictures: "",
        created: true,
      });
    }
  }

  handleChangeAuthor(event) {
    const value = event.target.value;
    this.setState({ author: value });
  }
  handleChangeTrip(event) {
    const value = event.target.value;
    this.setState({ selectedTrip: value });
    console.log("state", this.state);
    console.log("value", value);
  }
  handleChangeDescription(event) {
    const value = event.target.value;
    this.setState({ description: value });
  }
  handleChangeContent(event) {
    const value = event.target.value;
    this.setState({ content: value });
  }
  handleChangeLodging(event) {
    const value = event.target.value;
    this.setState({ lodging: value });
  }
  handleChangeRecRestaurants(event) {
    const value = event.target.value;
    this.setState({ rec_restaurants: value });
  }
  handleChangePictures(event) {
    const value = event.target.value;
    this.setState({ pictures: value });
  }

  render() {
    if (!this.props.token) {
      return (
        <div className="px-4 py-5 my-5 text-center">
          You do not have access to this page. Please
          <Link to="/login"> login </Link>
          to view this content.
        </div>
      );
    }
    return (
      <div className="form-group">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a Blog</h1>
            <form onSubmit={this.handleSubmit} id="create-review-form">
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleChangeAuthor}
                  value={this.state.author}
                  placeholder="Author"
                  required
                  type="text"
                  name="author"
                  id="author"
                  className="form-control"
                />
                <label htmlFor="author">Author</label>
              </div>
              <div className="mb-3">
                <select
                  onChange={this.handleChangeTrip}
                  value={this.state.selectedTrip}
                  placeholder="Trip"
                  required
                  name="trip"
                  id="trip"
                  className="form-select"
                >
                  <option value="">Choose a Trip</option>
                  {this.state.trip.map((trip) => {
                    return (
                      <option key={trip.id} value={trip.title}>
                        {" "}
                        {trip.title}
                      </option>
                    );
                  })}
                </select>
              </div>
              <div className="form-group">
                <textarea
                  rows="3"
                  onChange={this.handleChangeDescription}
                  value={this.state.description}
                  placeholder="Short Description"
                  required
                  type="text"
                  name="description"
                  id="description"
                  className="form-control"
                />
                <label htmlFor="description">Description</label>
              </div>
              <div className="form-group">
                <textarea
                  rows="8"
                  onChange={this.handleChangeContent}
                  value={this.state.content}
                  placeholder="Start writing your Blog here."
                  required
                  type="text"
                  name="content"
                  id="content"
                  className="form-control"
                />
                <label htmlFor="content">Content</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleChangeLodging}
                  value={this.state.lodging}
                  placeholder="Lodging"
                  required
                  type="text"
                  name="lodging"
                  id="lodging"
                  className="form-control"
                />
                <label htmlFor="lodging">Lodging</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleChangeRecRestaurants}
                  value={this.state.rec_restaurants}
                  placeholder="Restaurants"
                  required
                  type="text"
                  name="rec_restaurants"
                  id="rec_restaurants"
                  className="form-control"
                />
                <label htmlFor="rec_restaurants">Recommended Restaurants</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleChangePictures}
                  value={this.state.pictures}
                  placeholder="Pictures"
                  required
                  type="text"
                  name="pictures"
                  id="pictures"
                  className="form-control"
                />
                <label htmlFor="pictures">Pictures</label>
              </div>
              <button className="btn btn-danger">Create</button>
            </form>
            {this.state.created && <Navigate to="/reviews"></Navigate>}
          </div>
        </div>
      </div>
    );
  }
}

export default BlogForm;
